#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; FAT file system driver in Scheme
;; Copyright © 2019, 2020 Göran Weinholt <goran@weinholt.se>

;; fs-fatfs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; fs-fatfs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

(import
  (rnrs (6))
  (fs fatfs)
  (fs fatfs port-device)
  (wak fmt))

(unless (= (length (command-line)) 2)
  (error #f "Usage: test-type-file <filesystem.img>"))

(define (fxtest x y)
  (not (eqv? 0 (fxand x y))))

(define (format-fi-atime fi)
  (let ((d (fatfs-file-info:raw-adate fi)))
    (let-values ([(Y M D) (fatfs-raw-date-components d)])
      (pad-char #\0
                (cat Y "-" (pad/left 2 M) "-" (pad/left 2 D))))))

(define (format-fi-ctime fi)
  (let ((d (fatfs-file-info:raw-cdate fi))
        (t (fatfs-file-info:raw-ctime fi))
        (ms (fatfs-file-info:raw-ctime-ms fi)))
    (let-values ([(Y M D)    (fatfs-raw-date-components d)]
                 [(h m s ms) (fatfs-raw-time-components t ms)])
      (pad-char #\0
                (cat Y "-" (pad/left 2 M) "-" (pad/left 2 D) " "
                     (pad/left 2 h) ":" (pad/left 2 m) ":" (pad/left 2 s)
                     "." (pad/left 3 ms))))))

(define (format-fi-mtime fi)
  (let ((d (fatfs-file-info:raw-wdate fi))
        (t (fatfs-file-info:raw-wtime fi)))
    (cond
      ((and (eqv? d 0) (eqv? t 0))
       (make-string (string-length "1980-01-01 00:00:00") #\space))
      (else
       (let-values ([(Y M D)    (fatfs-raw-date-components d)]
                    [(h m s _)  (fatfs-raw-time-components t)])
         (pad-char #\0
                   (cat Y "-" (pad/left 2 M) "-" (pad/left 2 D) " "
                        (pad/left 2 h) ":" (pad/left 2 m) ":" (pad/left 2 s))))))))

(define (format-cluster-list lst)
  (let lp ((lst lst))
    (cond ((null? lst)
           (cat))
          ((null? (cdr lst))
           (cat "<" (car lst) ">"))
          (else
           (let ((start (car lst)))
             (let lp* ((prev start) (lst (cdr lst)))
               (cond ((null? lst)
                      (cat "<" start "-" prev ">"))
                     ((= (car lst) (+ prev 1))
                      (lp* (car lst) (cdr lst)))
                     ((= start prev)
                      (cat "<" start "> " (lp lst)))
                     (else
                      (cat "<" start "-" prev "> " (lp lst))))))))))

(define (ls/R fs path depth)
  (let ((rev-path (reverse path)))
    (let ((dir (fatfs-open-directory fs path)))
      (let lp ()
        (let ((stat (fatfs-read-directory dir)))
          (cond
            ((eof-object? stat)
             (fatfs-close-directory dir))
            (else
             (let* ((name (fatfs-file-info:filename stat))
                    (attr (fatfs-file-info:attr stat))
                    (full-name (reverse (cons name rev-path))))
               (fmt #t (space-to (* depth 2))
                    (pad/right 13 name)
                    (space-to 22) " "
                    (if (fatfs-file-info-directory? stat)
                        (pad/right 9 "<DIR>")
                        (pad/left 9 (fatfs-file-info:size stat)))
                    " "
                    (if (fxtest attr FATFS-READ-ONLY) "R" "-")
                    (if (fxtest attr FATFS-HIDDEN)    "H" "-")
                    (if (fxtest attr FATFS-SYSTEM)    "S" "-")
                    (if (fxtest attr FATFS-VOLUME-ID) "V" "-")
                    (if (fxtest attr FATFS-DIRECTORY) "D" "-")
                    (if (fxtest attr FATFS-ARCHIVE)   "A" "-")
                    " " (format-fi-mtime stat)
                    ;; " S:"
                    ;; (* (fatfs-bytes/sector fs) (fatfs-sectors/cluster fs)
                    ;;    (length (fatfs-cluster-list fs (fatfs-file-info:cluster stat))))
                    (cond
                      ((fatfs-file-info:long-filename stat) =>
                       (lambda (fn) (cat " " (wrt fn))))
                      (else (cat)))
                    " " (format-cluster-list (fatfs-cluster-list fs (fatfs-file-info:cluster stat)))
                    nl)
               (when #f
                 (fmt #t
                      (space-to (+ 22 10)) "Access: " (format-fi-atime stat) nl
                      (space-to (+ 22 10)) "Create: " (format-fi-ctime stat)
                      nl))
               (when (and (fatfs-file-info-directory? stat)
                          (not (member name '("." ".."))))
                 (ls/R fs full-name (+ depth 1)))
               (lp)))))))))

(define (format-volume-serial id)
  (let ((x (fmt #f (pad-char #\0 (pad 8 (radix 16 (num id)))))))
    (string-append (substring x 0 4) "-" (substring x 4 8))))

(let ((fs (open-fatfs (open-file-fatfs-device/read-only (cadr (command-line)) 512))))
  (let ((volname (fatfs-volume-name fs))
        (volid (fatfs-volume-id fs)))
    (fmt #t "Volume Label: " (wrt volname) nl
         "Volume Serial Number: " (and volid (format-volume-serial volid)) nl
         nl))

  (ls/R fs '() 0)

  (fmt #t
       (pad/left 50
                 (num (* (fatfs-bytes/sector fs)
                         (fatfs-sectors/cluster fs)
                         (fatfs-total-clusters fs))
                      10 #f #f 3 " ")
                 " bytes total")
       nl
       (pad/left 49 (num (fatfs-bytes-free fs) 10 #f #f 3 " ")
                 " bytes free")
       nl))

(flush-output-port (current-output-port))
