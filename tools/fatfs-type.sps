#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; This file is part of fs-fatfs, a FAT file system driver in Scheme
;; Copyright © 2019, 2020 Göran Weinholt <goran@weinholt.se>

;; fs-fatfs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; fs-fatfs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;; Open a file and write it to the terminal

(import
  (rnrs (6))
  (fs fatfs)
  (fs fatfs port-device))

(when (< (length (command-line)) 3)
  (error #f "Usage: test-type-file <filesystem.img> [<dir>...] <filename>"))

(let ((x (open-fatfs (open-file-fatfs-device/read-only (cadr (command-line)) 512))))
  (call-with-port (standard-output-port)
    (lambda (outp)
      (call-with-port (fatfs-open-file x (cddr (command-line)) fatfs-open/read)
        (lambda (port)
          (let lp ()
            (let ((bytes (get-bytevector-n port 4096)))
              (unless (eof-object? bytes)
                (put-bytevector outp bytes)
                (lp)))))))))
