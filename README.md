# (fs fatfs)

R6RS Scheme library for reading and writing FAT filesystems. Supports
FAT12, FAT16, FAT32 with VFAT extensions.

## API

The API is close to SRFI 170, with adaptations for FAT. Full
documentation is pending.

## Installation

Use [Akku.scm](https://akkuscm.org) and install into your project with
`akku install fs-fatfs`.

## Limitations

* Currently only code page cp437 (US) is supported.
* Date and time is not set on files.
* Concurrent accesses to files is not safe. Examples of unsafe use:
  deleting an open file, renaming a directory with an open file, and
  multiple writers to the same file.
* The file system sector size needs to match the device logical sector
  size.

These limitations are planned to be fixed in future versions.

## Portability

Works on conforming R6RS Scheme implementations. See the GitLab CI
runs for current test data.

Known problems:

* Ikarus - missing `open-file-input/output-port`, known ancient issue.
  Can be worked around if only read support is desired.
* Racket - can't set the port position on input/output ports.
  Can be worked around if only read support is desired.
* Vicare - hangs while writing 0 bytes to a custom binary output port.
  Triggered by an initial write of 1024 bytes where the port only accepts
  512 bytes. Can be worked around if only read support is desired.

## License

fs-fatfs is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fs-fatfs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
