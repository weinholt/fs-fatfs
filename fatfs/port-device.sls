;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of fs-fatfs, a FAT file system driver in Scheme
;; Copyright © 2018-2020 Göran Weinholt <goran@weinholt.se>

;; fs-fatfs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; fs-fatfs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; Device emulation on top of binary input/output ports

(library (fs fatfs port-device)
  (export
    port->fatfs-device
    open-file-fatfs-device
    open-file-fatfs-device/read-only)
  (import
    (rnrs)
    (fs fatfs))

(define (port->fatfs-device port logical-sector-size)
  (define (read-sectors sector-idx sectors)
    (let ((idx (* sector-idx logical-sector-size)))
      (set-port-position! port idx)
      (let* ((len (* logical-sector-size sectors))
             (bv (get-bytevector-n port len)))
        (unless (and (bytevector? bv) (eqv? (bytevector-length bv) len))
          (error 'read-sectors "Short read" port sector-idx sectors))
        bv)))

  (define (write-sectors sector-idx data)
    (unless (output-port? port)
      (assertion-violation 'write-sectors "Opened in read-only mode" port sector-idx))
    (unless (fxzero? (fxmod (bytevector-length data) logical-sector-size))
      (assertion-violation 'write-sectors "Bad write length" sector-idx (bytevector-length data)))
    (set-port-position! port (* sector-idx logical-sector-size))
    (put-bytevector port data))

  (define (flush)
    (when (output-port? port)
      (flush-output-port port)))

  (define (close)
    (close-port port))

  (unless (port-has-set-port-position!? port)
    (assertion-violation 'port->fatfs-device "The port must support set-port-position!" port))

  (make-fatfs-device logical-sector-size
                     read-sectors
                     write-sectors
                     flush
                     close))

(define (open-file-fatfs-device filename logical-sector-size)
  (let ((port (open-file-input/output-port filename (file-options no-fail no-truncate))))
    (port->fatfs-device port logical-sector-size)))

(define (open-file-fatfs-device/read-only filename logical-sector-size)
  (let ((port (open-file-input-port filename)))
    (port->fatfs-device port logical-sector-size))))
