;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of fs-fatfs, a FAT file system driver in Scheme
;; Copyright © 2018, 2019, 2020 Göran Weinholt <goran@weinholt.se>

;; fs-fatfs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; fs-fatfs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; FAT filesystem

(library (fs fatfs directory)
  (export
    make-directory-lister

    attr-dir? attr-volname? attr-regular?

    FATFS-READ-ONLY
    FATFS-HIDDEN
    FATFS-SYSTEM
    FATFS-VOLUME-ID
    FATFS-DIRECTORY
    FATFS-ARCHIVE)
  (import
    (rnrs)
    (rnrs mutable-strings)
    (struct pack)
    (fs fatfs compat)
    #;
    (industria hexdump))

(define FATFS-READ-ONLY #b00000001)
(define FATFS-HIDDEN    #b00000010)
(define FATFS-SYSTEM    #b00000100)
(define FATFS-VOLUME-ID #b00001000)
(define FATFS-DIRECTORY #b00010000)
(define FATFS-ARCHIVE   #b00100000)

(define FATFS-LONG-FILENAME
  (fxior FATFS-READ-ONLY FATFS-HIDDEN FATFS-SYSTEM FATFS-VOLUME-ID))

(define (attr-dir? attr)
  (fx=? FATFS-DIRECTORY
        (fxand (fxior FATFS-DIRECTORY FATFS-VOLUME-ID)
               attr)))

(define (attr-volname? attr)
  (fx=? FATFS-VOLUME-ID
        (fxand (fxior FATFS-DIRECTORY FATFS-VOLUME-ID)
               attr)))

(define (attr-regular? attr)
  (eqv? 0 (fxand (fxior FATFS-DIRECTORY FATFS-VOLUME-ID)
                 attr)))

(define (attr-long-name? attr)
  (eqv? FATFS-LONG-FILENAME
        (fxand attr
               (fxior FATFS-READ-ONLY FATFS-HIDDEN FATFS-SYSTEM FATFS-VOLUME-ID
                      FATFS-DIRECTORY FATFS-ARCHIVE))))

(define (utf16z->string bv)             ;XXX: UCS-2 really
  (let ((len (do ((i 0 (fx+ i 2)))
                 ((or (fx=? i (bytevector-length bv))
                      (fxzero? (bytevector-u16-native-ref bv i)))
                  (fxdiv i 2)))))
    (substring (utf16->string* bv (endianness little) #t)
               0 len)))

(define (entry-checksum sector offset)
  (do ((i 0 (fx+ i 1))
       (checksum 0 (fxand #xff (fx+ (bytevector-u8-ref sector (fx+ offset i))
                                    (fxrotate-bit-field checksum 0 8 7)))))
      ((fx=? i 11)
       checksum)))

(define (entry-volume-name sector offset codepage)
  (call-with-string-output-port
    (lambda (p)
      (let lp ((i 0))
        (unless (fx=? i 11)
          (let* ((b (bytevector-u8-ref sector (fx+ offset i)))
                 (c (string-ref codepage b)))
            (put-char p c)
            (lp (fx+ i 1))))))))

(define (entry-short-filename sector offset codepage downcase-filename downcase-extension)
  (call-with-string-output-port
    (lambda (p)
      (let lp ((i 0))
        (unless (fx=? i 11)
          (let ((b (bytevector-u8-ref sector (fx+ offset i))))
            (when (and (fx=? i 8)
                       (not (= #x20 (bytevector-u8-ref sector (fx+ offset 8)))))
              (put-char p #\.))
            (cond
              ((and (fx=? i 0) (fx=? b #x05))
               ;; #xe5 is used to mark unused/deleted entries, so this
               ;; is the replacement character.
               (put-char p (string-ref codepage #xe5)))
              (else
               (unless (fx=? b #x20)
                 (let* ((c (string-ref codepage b))
                        (c (if (or (and downcase-filename (fx<? i 8))
                                   (and downcase-extension (fx>=? i 8)))
                               (char-downcase c)
                               c)))
                   (put-char p c)))
               (lp (fx+ i 1))))))))))

(define (string-index-right str pred)
  (let lp ((i (fx- (string-length str) 1)))
    (cond ((eqv? i -1) #f)
          ((pred (string-ref str i)) i)
          (else (lp (fx- i 1))))))

(define invalid-in-short-name           ;plus the control characters
  '(#\" #\* #\+ #\, #\. #\/ #\: #\; #\< #\= #\> #\? #\[ #\\ #\] #\|))

(define invalid-in-long-name
  '(#\" #\* #\/ #\: #\< #\> #\? #\\ #\|))

(define (name+ext fn)
  (let* ((dot (string-index-right fn (lambda (c) (eqv? c #\.))))
         (name (if dot (substring fn 0 dot) fn))
         (ext (if dot (substring fn (fx+ dot 1) (string-length fn)) "")))
    (values name ext)))

;; Create a short filename entry (8.3), if possible. Returns the entry
;; as a bytevector or #f if the name is not valid for a short entry.
;; This also returns #f for some valid short names that would lose
;; their case information. Short names must be stored in upper case or
;; DOS will be unable to find them, but there are flags for downcasing
;; the name and the extension.
(define (encode-short-directory-entry codepage lfn attr start-cluster file-length)
  (define (char-need-lc? x) (and (char-alphabetic? x) (char-lower-case? x)))
  (define (char-need-uc? x) (and (char-alphabetic? x) (char-upper-case? x)))
  (define inverse-codepage
    (make-eqv-hashtable))
  (do ((i 0 (fx+ i 1)))
      ((fx=? i (string-length codepage)))
    (hashtable-set! inverse-codepage (string-ref codepage i) i))
  (let ((short (make-bytevector 32 0)))
    (do ((i 0 (fx+ i 1)))
        ((fx=? i 11))
      (bytevector-u8-set! short i (char->integer #\space)))
    (let-values ([(name ext) (name+ext lfn)])
      (let* ((name-lc? (string-index-right name char-need-lc?))
             (ext-lc? (string-index-right ext char-need-lc?)))
        (cond
          ((eqv? (string-length name) 0) #f)
          ((fx>? (string-length name) 8) #f)
          ((eqv? (string-length name) 0) #f)
          ((fx>? (string-length ext) 3) #f)
          ((string-index-right (string-append name ext)
                               (lambda (c)
                                 (or (fx<? (char->integer c) #x20)
                                     (memv c invalid-in-short-name)
                                     (not (hashtable-ref inverse-codepage
                                                         (char-upcase c) #f)))))
           #f)
          ((or (and name-lc? (string-index-right name char-need-uc?))
               (and ext-lc? (string-index-right ext char-need-uc?)))
           #f)                            ;mixed case
          (else
           (do ((i 0 (fx+ i 1)))
               ((fx=? i (string-length name)))
             (bytevector-u8-set! short i (hashtable-ref inverse-codepage
                                                        (char-upcase (string-ref name i))
                                                        #f)))
           (when ext
             (do ((i 0 (fx+ i 1)))
                 ((fx=? i (string-length ext)))
               (bytevector-u8-set! short (fx+ i 8)
                                   (hashtable-ref inverse-codepage
                                                  (char-upcase (string-ref ext i))
                                                  #f))))
           (let ((caseconv (fxior (if name-lc? (fxarithmetic-shift-left 1 3) 0)
                                  (if ext-lc? (fxarithmetic-shift-left 1 4) 0)))
                 (ctime-ms 0) (ctime 0) (cdate 0) (adate 0)
                 (cluster-high (bitwise-bit-field start-cluster 16 32))
                 (wtime 0) (wdate 0)
                 (cluster-low (bitwise-bit-field start-cluster 0 16)))
             (pack! "<uC C C3S S 2S S L" short 11
                    attr caseconv ctime-ms ctime cdate adate
                    cluster-high wtime wdate cluster-low file-length)
             short)))))))

;; Take a long filename and construct the name that will go in the
;; short (8.3) filename entry.
(define (make-short-name codepage lfn used-short-names)
  (define inverse-codepage (make-eqv-hashtable))
  (do ((i 0 (fx+ i 1)))
      ((fx=? i (string-length codepage)))
    (hashtable-set! inverse-codepage (string-ref codepage i) i))
  (let-values ([(oem-name lossy?)
                ;; Replace characters that are not in the char-set with #\_.
                (let ((oem-name (make-string (string-length lfn))))
                  (let lp ((i 0) (lossy? #f))
                    (if (fx=? i (string-length lfn))
                        (values oem-name lossy?)
                        (let ((c (char-upcase (string-ref lfn i))))
                          (cond ((or (not (hashtable-ref inverse-codepage c #f))
                                     (and (not (eqv? c #\.)) (memv c invalid-in-short-name)))
                                 (string-set! oem-name i #\_)
                                 (lp (fx+ i 1) #t))
                                (else
                                 (string-set! oem-name i c)
                                 (lp (fx+ i 1) lossy?)))))))])
    ;; Remove all #\space and leading #\.
    (let ((oem-name (call-with-string-output-port
                      (lambda (p)
                        (let lp ((i 0) (leading? #t))
                          (unless (fx=? i (string-length oem-name))
                            (let ((c (string-ref oem-name i)))
                              (cond ((or (and leading? (eqv? c #\.))
                                         (eqv? c #\space))
                                     (lp (fx+ i 1) leading?))
                                    (else
                                     (put-char p c)
                                     (lp (fx+ i 1) #f))))))))))
      ;; Construct the 8.3 name without the ~1 part
      (let ((name8.3 (call-with-string-output-port
                       (lambda (p)
                         (let lp ((i 0))
                           (unless (or (eqv? i 8) (fx=? i (string-length oem-name)))
                             (let ((c (string-ref oem-name i)))
                               (unless (or (eqv? c #\.))
                                 (put-char p c)
                                 (lp (fx+ i 1))))))
                         (let ((dot (string-index-right oem-name (lambda (c) (eqv? c #\.)))))
                           (when (and dot (not (fx=? dot (fx- (string-length oem-name) 1))))
                             (put-char p #\.)
                             (let lp ((i (fx+ dot 1)) (n 0))
                               (unless (or (eqv? n 3) (fx=? i (string-length oem-name)))
                                 (let ((c (string-ref oem-name i)))
                                   (put-char p c)
                                   (lp (fx+ i 1) (fx+ n 1)))))))))))
        (cond
          ((and (not lossy?)
                (or (string=? oem-name name8.3)
                    (string=? oem-name (string-append name8.3 ".")))
                (not (hashtable-ref used-short-names name8.3 #f)))
           name8.3)
          (else
           (let-values ([(name ext) (name+ext name8.3)])
             (let lp ((i 1))
               (let* ((n (number->string i))
                      (fn (string-append (substring name 0
                                                    (fxmax 0
                                                           (fxmin (string-length name)
                                                                  (fx- 8 (fx+ 1 (string-length n))))))
                                         "~" n
                                         (if (string=? ext "") "" ".")
                                         ext)))
                 (if (hashtable-ref used-short-names fn #f)
                     (lp (fx+ i 1))
                     fn))))))))))

;; Convert a filename from a string to UCS-2 with NUL termination and
;; U+FFFF padding.
(define (filename->utf16zp lfn)
  (define multiple (fx* 2 13))          ;13 chars per entry, 2 bytes per char
  (string-for-each
   (lambda (c)
     (when (eqv? c #\nul)
       (assertion-violation 'filename->utf16zp "Unrepresentable filename" lfn))
     (when (memv c invalid-in-long-name)
       (assertion-violation 'filename->utf16zp "Invalid character in filename" lfn c)))
   lfn)
  (when (not (string-index-right lfn (lambda (c) (not (eqv? c #\.)))))
    (assertion-violation 'filename->utf16zp "Invalid filename" lfn))
  (let* ((filename (string->utf16 lfn (endianness little)))
         (rem (fxmod (bytevector-length filename) multiple)))
    (when (fxzero? (bytevector-length filename))
      (assertion-violation 'filename->utf16zp "Empty filename" lfn))
    (when (fx>? (bytevector-length filename) (* 255 2))
      (assertion-violation 'filename->utf16zp "Filename too long" lfn))
    (if (fxzero? rem)
        filename
        (let* ((fnlen (bytevector-length filename))
               (bv (make-bytevector (fx+ fnlen (fx- multiple rem)) #xff)))
          (bytevector-copy! filename 0 bv 0 fnlen)
          (bytevector-u8-set! bv fnlen 0) ;NUL
          (bytevector-u8-set! bv (fx+ fnlen 1) 0)
          bv))))

(define (encode-long-directory-entries codepage lfn attr start-cluster file-length used-short-names)
  (define name-bytes/entry (fx* 2 13))
  (let* ((filename (filename->utf16zp lfn))
         (entries (fxdiv (bytevector-length filename) name-bytes/entry))
         (ret (make-bytevector (fx* (fx+ entries 1) 32) 0))
         (short-name (make-short-name codepage lfn used-short-names))
         (short-entry (encode-short-directory-entry codepage short-name attr start-cluster file-length))
         (csum (entry-checksum short-entry 0)))
    (do ((i 0 (fx+ i 1))
         (offset (fx- (bytevector-length ret) (fx* 32 2)) (fx- offset 32))
         (name-offset 0 (fx+ name-offset name-bytes/entry)))
        ((fx=? i entries)
         (bytevector-copy! short-entry 0 ret (fx- (bytevector-length ret) 32) 32)
         ret)
      (pack! "<C 10x CxC 2x 10x 2x" ret offset
             (if (fx=? i (fx- entries 1)) (fxior #x40 (fx+ i 1)) (fx+ i 1))
             FATFS-LONG-FILENAME csum)
      (bytevector-copy! filename name-offset ret (fx+ offset 1) 10)
      (bytevector-copy! filename (fx+ name-offset 10) ret (fx+ offset 14) 12)
      (bytevector-copy! filename (fx+ name-offset 22) ret (fx+ offset 28) 4))))

;; Scan the cluster chain for the largest free space. Makes no effort
;; to defrag directories (which is a job left to disk utilities).
;; Might span multiple sectors.
;; Returns:
;;  * start sector index
;;  * start entry index
;;  * #t if an end marker should be written
(define (allocate-directory-space cluster-chain num-entries)
  (cluster-chain 'rewind)
  (let ((sector0 (cluster-chain 'get-sector)))
    (let lp-sector ((sector sector0) (start-idx 0) (entry-idx 0) (free-entries 0) (seen-end? #f))
      (if (eof-object? sector)
          (cond ((cluster-chain 'space-available?)
                 (when (>= (+ entry-idx num-entries) #xffff)
                   (error 'directory-create-entry
                          "Too many directory entries" cluster-chain))
                 (cluster-chain 'extend!)
                 (lp-sector (make-bytevector (bytevector-length sector0) 0) start-idx entry-idx free-entries seen-end?))
                (else
                 (error 'directory-create-entry
                        "Out of space for directory entries")))
          (let lp ((offset 0) (start-idx start-idx) (entry-idx entry-idx) (free-entries free-entries) (seen-end? seen-end?))
            (cond ((fx=? offset (bytevector-length sector))
                   (cluster-chain 'step-forward)
                   (lp-sector (cluster-chain 'get-sector) start-idx entry-idx free-entries seen-end?))
                  ((memv (bytevector-u8-ref sector offset) '(0 #xE5))
                   (let ((free-entries (fx+ free-entries 1)))
                     (if (fx=? free-entries num-entries)
                         (let ((write-end-mark?
                                (if (not seen-end?)
                                    #f
                                    (if (fx=? (bytevector-length sector) (fx+ offset 32))
                                        (not (cluster-chain 'at-end?))
                                        #t))))
                           (let-values ([(sector-offset entry-offset)
                                         (fxdiv-and-mod (fx* start-idx 32) (bytevector-length sector0))])
                             (values sector-offset (fxdiv entry-offset 32) write-end-mark?)))
                         (lp (fx+ offset 32) start-idx (fx+ entry-idx 1) free-entries
                             (or seen-end? (eqv? 0 (bytevector-u8-ref sector offset)))))))
                  (else
                   (lp (fx+ offset 32) (fx+ entry-idx 1) (fx+ entry-idx 1) 0 #f))))))))

;; Create a directory entry in an existing directory. The cluster
;; chain is positioned at some unspecified position after this.
(define (directory-create-entry codepage lfn attr start-cluster file-length
                                cluster-chain used-short-names)
  (cond
    ((encode-short-directory-entry codepage lfn attr start-cluster file-length) =>
     (lambda (short)
       ;; XXX: Assumes the caller checked that the name does not exist
       (cluster-chain 'rewind)
       (let ((sector0 (cluster-chain 'get-sector)))
         (let lp-sector ((sector sector0) (num-entries 0))
           (if (eof-object? sector)
               (cond ((>= num-entries #xffff)
                      (error 'directory-create-entry
                             "Too many directory entries" lfn))
                     ((cluster-chain 'space-available?)
                      (cluster-chain 'extend!)
                      (lp-sector (make-bytevector (bytevector-length sector0) 0) num-entries))
                     (else
                      (error 'directory-create-entry
                             "No free space for more directory entries" lfn)))
               (let lp ((offset 0) (num-entries num-entries))
                 (cond ((fx=? offset (bytevector-length sector))
                        (cluster-chain 'step-forward)
                        (lp-sector (cluster-chain 'get-sector) num-entries))
                       ((memv (bytevector-u8-ref sector offset) '(0 #xE5))
                        (let ((at-end? (eqv? (bytevector-u8-ref sector offset) 0)))
                          (bytevector-copy! short 0 sector offset (bytevector-length short))
                          (when at-end?
                            ;; Put the end mark on the next entry.
                            (unless (fx=? (bytevector-length sector) (fx+ offset 32))
                              (bytevector-u8-set! sector (fx+ offset (fx+ 32 1)) 0)))
                          (cluster-chain 'put-sector sector)
                          (when (and at-end?
                                     (fx=? (bytevector-length sector) (fx+ offset 32))
                                     (not (cluster-chain 'at-end?)))
                            ;; Put the end mark on the next sector.
                            (cluster-chain 'step-forward)
                            (let ((sector (cluster-chain 'get-sector)))
                              (bytevector-u8-set! sector 0 0)
                              (cluster-chain 'put-sector sector)))))
                       (else
                        (lp (fx+ offset 32) (fx+ num-entries 1))))))))))
    (else
     ;; Long filename entries and one short name entry.
     (let* ((entries (encode-long-directory-entries codepage lfn attr start-cluster
                                                    file-length used-short-names))
            (num-entries (fxdiv (bytevector-length entries) 32)))
       (let-values ([(sector-idx entry-idx write-end-mark?)
                     (allocate-directory-space cluster-chain num-entries)])
         ;; Seek to the sector
         (cluster-chain 'rewind)
         (do ((i 0 (fx+ i 1)))
             ((fx=? i sector-idx))
           (cluster-chain 'step-forward))
         ;; Write out the entries, one sector at a time
         (let ((sector0 (cluster-chain 'get-sector)))
           (let lp ((sector sector0)
                    (num-entries num-entries)
                    (sector-offset (fx* entry-idx 32))
                    (entries-offset 0))
             (let ((write-entries (fxmin num-entries
                                         (fxdiv (fx- (bytevector-length sector) sector-offset) 32))))
               (bytevector-copy! entries entries-offset
                                 sector sector-offset
                                 (fx* write-entries 32))
               (let ((sector-offset (fx+ sector-offset (fx* write-entries 32)))
                     (num-entries (fx- num-entries write-entries)))
                 (when (and (eqv? num-entries 0)
                            write-end-mark?
                            (not (fx=? sector-offset (bytevector-length sector))))
                   (bytevector-u8-set! sector sector-offset 0)) ;end marker
                 (cluster-chain 'put-sector sector)
                 (cond
                   ((eqv? num-entries 0)
                    (when (and write-end-mark? (fx=? sector-offset (bytevector-length sector)))
                      (cluster-chain 'step-forward)
                      (let ((sector (cluster-chain 'get-sector)))
                        (bytevector-u8-set! sector 0 0) ;end marker
                        (cluster-chain 'put-sector sector))))
                   (else
                    (cluster-chain 'step-forward)
                    (lp (cluster-chain 'get-sector) num-entries 0
                        (fx+ entries-offset (fx* write-entries 32))))))))))))))

(define (make-directory-lister codepage cluster-chain)
  ;; Sector and offset for the current entry.
  (define sector #f)                    ;bytevector
  (define offset 0)
  ;; Sector, start offset and length (in 32-byte entries) for the
  ;; start of the long file name.
  (define entry-pos #f)
  (define entry-offset #f)
  (define entry-len #f)
  (define lfn-buf (make-bytevector 520 0))
  (define used-short-names (make-hashtable string-hash string=?))
  (case-lambda
    (()                         ;reads a complete directory entry
     (let lp-sector ((lfn-expect #f) (lfn-check #f))
       (cond
         ((not sector)
          (set! sector (cluster-chain 'get-sector))
          (set! offset 0)
          (lp-sector lfn-expect lfn-check))
         ((eof-object? sector)
          (values (eof-object) #f #f #f #f
                  #f #f #f #f #f #f))
         (else
          (let lp-dirent ((lfn-expect lfn-expect) (lfn-check lfn-check))
            (cond
              ((fx=? offset (bytevector-length sector))
               (cluster-chain 'step-forward)
               (set! sector (cluster-chain 'get-sector))
               (set! offset 0)
               (lp-sector lfn-expect lfn-check))
              ((fx=? (bytevector-u8-ref sector offset) 0) ;end of dir
               (values (eof-object) #f #f #f #f
                       #f #f #f #f #f #f))
              (else
               (let ((attr (bytevector-u8-ref sector (fx+ offset 11))))
                 (cond
                   ((fx=? (bytevector-u8-ref sector offset) #xe5) ;deleted
                    (set! entry-pos #f)
                    (set! offset (fx+ offset 32))
                    (lp-dirent #f #f))
                   ((and (attr-long-name? attr)
                         (fx=? (bytevector-u8-ref sector (fx+ offset 12)) #x00)) ;entry type
                    ;; Long filenames.
                    (let-values ([(seq check) (unpack "C12xC" sector offset)])
                      (let ((idx (fxbit-field seq 0 5))
                            (last? (fxbit-set? seq 6)))
                        (cond
                          ;; Some sanity checks: entries are in
                          ;; decreasing sequence and the checksum is
                          ;; the same as the previous entry.
                          ((and (or (not lfn-check) (fx=? lfn-check check))
                                (or last? (and (fixnum? lfn-expect) (fx=? idx lfn-expect))))
                           (when last?
                             (set! entry-pos (cluster-chain 'get-position))
                             (set! entry-offset offset)
                             (set! entry-len 0)
                             (bytevector-fill! lfn-buf 0))
                           (let ((buf-offset (fx* (fx- idx 1) (* 13 2))))
                             (bytevector-copy! sector (fx+ offset 1)
                                               lfn-buf buf-offset
                                               10)
                             (bytevector-copy! sector (fx+ offset 14)
                                               lfn-buf (fx+ buf-offset 10)
                                               12)
                             (bytevector-copy! sector (fx+ offset 28)
                                               lfn-buf (fx+ buf-offset 22)
                                               4))
                           (set! offset (fx+ offset 32))
                           (set! entry-len (fx+ entry-len 1))
                           (lp-dirent (fx- idx 1) check))
                          (else          ;ignore an invalid entry
                           (set! entry-pos #f)
                           (set! offset (fx+ offset 32))
                           (lp-dirent #f #f))))))
                   (else
                    (let-values ([(caseconv ctime-ms ctime cdate adate
                                            cluster-high wtime wdate cluster-low file-length)
                                  (unpack "<11x x C C3S S 2S S L" sector offset)])
                      (let ((cluster (bitwise-ior
                                      (bitwise-arithmetic-shift-left cluster-high 16)
                                      cluster-low))
                            (fn (if (attr-volname? attr)
                                    (entry-volume-name sector offset codepage)
                                    (entry-short-filename sector offset codepage
                                                          (fxbit-set? caseconv 3)
                                                          (fxbit-set? caseconv 4))))
                            (lfn (and (eqv? lfn-expect 0)
                                      (fx=? lfn-check (entry-checksum sector offset))
                                      (utf16z->string lfn-buf))))
                        (when (not lfn)
                          (set! entry-pos #f))
                        (hashtable-set! used-short-names fn #t)
                        (set! offset (fx+ offset 32))
                        (values fn lfn cluster attr file-length
                                ctime-ms ctime cdate adate wtime wdate)))))))))))))
    ((cmd . arg*)
     (case cmd
       ((update)
        ;; Update the current entry. Positioned at immediately after
        ;; the entry. Not used for renaming.
        (let-values ([(attr caseconv ctime-ms ctime cdate adate
                            cluster-high wtime wdate cluster-low file-length)
                      (unpack "<uC C C3S S 2S S L" sector (fx+ 11 (fx- offset 32)))])
          (let lp ((arg* arg*))
            (unless (null? arg*)
              (let ((cmd (car arg*)) (arg (cadr arg*)))
                (case cmd
                  ((cluster)
                   (set! cluster-low (bitwise-bit-field arg 0 16))
                   (set! cluster-high (bitwise-bit-field arg 16 32)))
                  ((file-length) (set! file-length arg))
                  ((attr)        (set! attr arg))
                  ((adate)       (set! adate arg))
                  ((wdate)       (set! wdate arg))
                  ((wtime)       (set! wtime arg))
                  ((cdate)       (set! cdate arg))
                  ((ctime)       (set! ctime arg))
                  ((ctime-ms)    (set! ctime-ms arg))
                  (else
                   (error #f "Unknown directory update command" cmd arg)))
                (lp (cddr arg*)))))
          ;; Write back the changed fields
          ;;(hexdump #f sector)
          (pack! "<uC C C3S S 2S S L" sector (fx+ 11 (fx- offset 32))
                 attr caseconv ctime-ms ctime cdate adate
                 cluster-high wtime wdate cluster-low file-length)
          ;;(hexdump #f sector)
          (cluster-chain 'put-sector sector)))
       ((delete)
        ;; Delete the entry. Positioned at immediately after the entry.
        (cond (entry-pos
               ;; Delete all long filename entries
               (cluster-chain 'set-position entry-pos)
               (let lp ((sector (cluster-chain 'get-sector))
                        (offset entry-offset)
                        (n (fx+ entry-len 1)))
                 (cond ((eqv? n 0)
                        (cluster-chain 'put-sector sector))
                       ((fx=? offset (bytevector-length sector))
                        (cluster-chain 'put-sector sector)
                        (cluster-chain 'step-forward)
                        (lp (cluster-chain 'get-sector) 0 n))
                       (else
                        (bytevector-u8-set! sector offset #xe5)
                        (lp sector (fx+ offset 32) (fx- n 1))))))
              (else
               (bytevector-u8-set! sector (fx- offset 32) #xe5)
               (cluster-chain 'put-sector sector))))
       ((create)
        ;; Create an entry. The caller has walked the whole directory
        ;; and not found the file name. Need to find a place to store
        ;; the entry.
        (let-values ([(lfn start-cluster attr file-length) (apply values arg*)])
          (directory-create-entry codepage lfn attr start-cluster file-length
                                  cluster-chain used-short-names))
        (set! offset 'invalidated))
       ((init)
        (let ((start-cluster (cluster-chain 'get-start-cluster))
              (parent-cluster (car arg*))
              (sector (cluster-chain 'get-sector))
              (spaces (make-bytevector 11 (char->integer #\space))))
          (bytevector-fill! sector 0)
          (let ((attr FATFS-DIRECTORY) (ctime-ms 0) (ctime 0)
                (cdate 0) (adate 0) (wtime 0) (wdate 0) (file-length 0))
            ;; .
            (bytevector-copy! spaces 0 sector 0 11)
            (bytevector-u8-set! sector 0 (char->integer #\.))
            (let ((cluster-high (bitwise-bit-field start-cluster 16 32))
                  (cluster-low (bitwise-bit-field start-cluster 0 16)))
              (pack! "<uC x C3S S 2S S L" sector 11
                     attr ctime-ms ctime cdate adate
                     cluster-high wtime wdate cluster-low file-length))
            ;; ..
            (bytevector-copy! spaces 0 sector 32 11)
            (bytevector-u8-set! sector 32 (char->integer #\.))
            (bytevector-u8-set! sector 33 (char->integer #\.))
            (let ((cluster-high (bitwise-bit-field parent-cluster 16 32))
                  (cluster-low (bitwise-bit-field parent-cluster 0 16)))
              (pack! "<uC x C3S S 2S S L" sector (fx+ 32 11)
                     attr ctime-ms ctime cdate adate
                     cluster-high wtime wdate cluster-low file-length)))
          (cluster-chain 'put-sector sector)))
       ((get-start-cluster)
        (cluster-chain 'get-start-cluster))
       (else (error #f "Unknown directory command" cmd arg*)))))))
