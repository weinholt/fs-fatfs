;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of fs-fatfs, a FAT file system driver in Scheme
;; Copyright © 2018-2020 Göran Weinholt <goran@weinholt.se>

;; fs-fatfs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; fs-fatfs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; FAT filesystem

;; Supports FAT12, FAT16, FAT32 and VFAT.

;; TODO:
;;   Protection against bad concurrent accesses
;;   Filesystem dirty/clean bit
;;   Hooks for system date and time

(library (fs fatfs)
  (export
    make-fatfs-device

    open-fatfs
    close-fatfs

    fatfs?
    fatfs-bytes/sector
    fatfs-sectors/cluster
    fatfs-total-clusters
    fatfs-volume-name
    fatfs-volume-id
    fatfs-bytes-free

    fatfs-file-exists?
    fatfs-delete-file

    fatfs-open-file fatfs-open/read fatfs-open/write fatfs-open/read+write fatfs-open/append
    fatfs-open/create fatfs-open/exclusive fatfs-open/truncate

    ;; fatfs-fdes->textual-input-port
    ;; fatfs-fdes->binary-input-port
    ;; fatfs-fdes->textual-output-port
    ;; fatfs-fdes->binary-output-port
    ;; fatfs-port-fdes fatfs-close-fdes

    fatfs-create-directory
    fatfs-rename-file
    fatfs-delete-directory
    fatfs-set-file-mode
    fatfs-set-file-timespecs
    fatfs-truncate-file

    fatfs-file-info
    fatfs-file-info?
    fatfs-file-info:filename
    fatfs-file-info:long-filename
    fatfs-file-info:attr
    fatfs-file-info:size
    fatfs-file-info:blksize
    fatfs-file-info:cluster
    fatfs-file-info:raw-adate
    fatfs-file-info:raw-wdate
    fatfs-file-info:raw-wtime
    fatfs-file-info:raw-cdate
    fatfs-file-info:raw-ctime
    fatfs-file-info:raw-ctime-ms
    fatfs-file-info-directory?
    fatfs-file-info-regular?

    ;; Bits that go into fatfs-file-info:attr
    FATFS-READ-ONLY
    FATFS-HIDDEN
    FATFS-SYSTEM
    FATFS-VOLUME-ID
    FATFS-DIRECTORY
    FATFS-ARCHIVE

    ;; For en/decoding fatfs-file-info:raw-?{time,date}
    fatfs-raw-date
    fatfs-raw-time
    fatfs-raw-date-components
    fatfs-raw-time-components

    fatfs-open-directory fatfs-read-directory fatfs-close-directory

    fatfs-cluster-list)
  (import
    (rnrs (6))
    (rnrs io ports)                     ;workaround for Guile
    (struct pack)
    (fs fatfs directory))

;; Device interface. See the manual for assumptions and requirements.
;; See (fs fatfs port-device) to use a binary port.
(define-record-type fatfs-device
  (sealed #t)
  (opaque #t)
  (fields logical-sector-size
          read-sectors
          write-sectors
          flush
          close))

(define (dev-read-sectors dev lba sectors)
  ((fatfs-device-read-sectors dev) lba sectors))

(define (dev-write-sectors dev lba data)
  ((fatfs-device-write-sectors dev) lba data))

(define (dev-flush dev)
  ((fatfs-device-flush dev)))

(define (dev-close dev)
  ((fatfs-device-close dev)))

(define (fxtest x y)
  (not (eqv? 0 (fxand x y))))

(define-record-type fatfs
  (sealed #t)
  (nongenerative fatfs-v0-34b32cf0-e410-48a0-875d-a178a45a06a0)
  (fields dev type bytes/sector sectors/cluster sectors/FAT
          FATs
          first-FAT-sector
          first-data-sector
          total-clusters
          root-directory-cluster        ;FAT32 only
          root-directory-sectors
          root-directory-entries
          codepage
          (mutable free-cluster-hint)))

(define-record-type (&fatfs-file-info make-fatfs-file-info fatfs-file-info?)
  (sealed #t)
  (nongenerative fatfs-file-info-v0-3c734eea-bcba-45df-ac7b-1a661b7c2b07)
  (fields (immutable filename      fatfs-file-info:filename)
          (immutable long-filename fatfs-file-info:long-filename)
          (immutable size          fatfs-file-info:size)
          (immutable blksize       fatfs-file-info:blksize)
          (immutable attr          fatfs-file-info:attr)
          (immutable cluster       fatfs-file-info:cluster)
          (immutable raw-adate     fatfs-file-info:raw-adate)
          (immutable raw-wdate     fatfs-file-info:raw-wdate)
          (immutable raw-wtime     fatfs-file-info:raw-wtime)
          (immutable raw-cdate     fatfs-file-info:raw-cdate)
          (immutable raw-ctime     fatfs-file-info:raw-ctime)
          (immutable raw-ctime-ms  fatfs-file-info:raw-ctime-ms)))

(define (fatfs-bytes/cluster fatfs)
  (* (fatfs-bytes/sector fatfs) (fatfs-sectors/cluster fatfs)))

(define (fatfs-minimum-valid-cluster _fatfs)
  2)

(define (fatfs-maximum-valid-cluster fatfs)
  (+ (fatfs-total-clusters fatfs) 1))

;; The end-of-cluster-chain value to put into the FAT.
(define (fatfs-end-of-cluster-chain fatfs)
  (case (fatfs-type fatfs)
    ((fat12) #xfff)
    ((fat16) #xffff)
    ((fat32) #xfffffff)
    (else
     (error 'fatfs-end-of-cluster-chain "Unsupported fs type" (fatfs-type fatfs)))))

(define (fatfs-raw-date y m d)
  (fxior (fxarithmetic-shift-left (fx- y 1980) 9)
         (fxarithmetic-shift-left m 5)
         d))

(define fatfs-raw-time
  (case-lambda
    ((h m s ms)
     (let-values ([(s^ s/2) (fxdiv-and-mod s 2)])
       (values (fxior (fxarithmetic-shift-left h 11)
                      (fxarithmetic-shift-left m 5)
                      s^)
               (fxdiv (fx+ ms (fx* s/2 1000))
                      10))))
    ((h m s)
     (let-values ([(t _ms) (fatfs-raw-time h m s 0)])
       t))))

;; Returns year, month, day
(define (fatfs-raw-date-components x)
  (values (fx+ 1980 (fxbit-field x 9 16))
          (fxbit-field x 5 9)
          (fxbit-field x 0 5)))

;; Returns hour, minute, seconds, milliseconds
(define fatfs-raw-time-components
  (case-lambda
    ((t) (fatfs-raw-time-components t 0))
    ((t tenth)
     (let-values ([(s ms) (fxdiv-and-mod tenth 100)])
       (values (fxbit-field t 11 16)
               (fxbit-field t 5 11)
               (fx+ s (fx* 2 (fxbit-field t 0 5)))
               (fx* ms 10))))))

(define (fatfs-file-info-directory? stat)
  (attr-dir? (fatfs-file-info:attr stat)))

(define (fatfs-file-info-regular? stat)
  (attr-regular? (fatfs-file-info:attr stat)))

(define (fatfs-file-info-volume-label? stat)
  (attr-volname? (fatfs-file-info:attr stat)))

(define cp437          ;FIXME: more code pages, codes < #x20 do not matter
  "\x2007;☺☻♥♦♣♠•◘○◙♂♀♪♫☼►◄↕‼¶§▬↨↑↓→←∟↔▲▼ !\"#$%&'()*+,-./0123456789:;<=>?\
@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~⌂\
ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜ¢£¥₧ƒáíóúñÑªº¿⌐¬½¼¡«»░▒▓│┤╡╢╖╕╣║╗╝╜╛┐└\
┴┬├─┼╞╟╚╔╩╦╠═╬╧╨╤╥╙╘╒╓╫╪┘┌█▄▌▐▀αβΓπΣσµτΦΘΩδ∞∅∈∩≡±≥≤⌠⌡÷≈°∙·√ⁿ²■\xA0;")

(define open-fatfs
  (case-lambda
    ((dev)
     (open-fatfs dev cp437))
    ((dev codepage)
     ;; Read the BPB block and parse the data.
     (let ((blk0 (guard (exn ((error? exn)
                              ;; Try the backup BPB
                              (dev-read-sectors dev 6 1)))
                   (dev-read-sectors dev 0 1))))
       #;
       (unless (= #xAA55 (unpack "<S" blk0 510))
         (error 'open-fatfs "Missing #xAA55 signature" dev))
       (let-values
           (((bytes/sector
              sectors/cluster reserved-sectors FATs root-directory-entries
              total-sectors _media-descriptor-type sectors/FAT
              _sectors/track _heads _hidden-sectors large-total-sectors)
             (unpack "<u11x SCSCSSCSSSLL" blk0))
            ;; FAT12/16
            ((_fat12-drive-number _fat12-flags fat12-signature fat12-serial)
             (unpack "<u36x CCCL 11x 8x" blk0))
            ;; FAT32
            ((fat32-sectors/FAT
              _fat32-flags fat32-version fat32-root-dir-cluster fat32-fsinfo-sector
              _fat32-backup-boot-sector _fat32-drive-number _fat32-nt-flags fat32-signature
              fat32-serial)
             (unpack "<u36x LSSLSS 12x CCCL 11x 8x" blk0)))
         ;; Compute the parameters needed during operation.
         (when (or (eqv? bytes/sector 0) (eqv? sectors/cluster 0))
           (error 'open-fatfs "Invalid FAT parameters" dev bytes/sector sectors/cluster))
         (let* ((total-sectors (if (zero? total-sectors) large-total-sectors total-sectors))
                (sectors/FAT (if (zero? sectors/FAT) fat32-sectors/FAT sectors/FAT))
                (root-directory-sectors
                 (div (+ (* 32 root-directory-entries) (- bytes/sector 1))
                      bytes/sector))
                (first-data-sector
                 (+ reserved-sectors (* sectors/FAT FATs) root-directory-sectors))
                (first-FAT-sector reserved-sectors)
                (total-data-sectors
                 (- total-sectors reserved-sectors (* sectors/FAT FATs)
                    root-directory-sectors))
                (total-clusters (div total-data-sectors sectors/cluster))
                (type (cond ((< total-clusters 4085) 'fat12)
                            ((< total-clusters 65525) 'fat16)
                            ((< total-clusters 268435445) 'fat32)
                            (else 'exfat)))
                ;; TODO: Read and write the hint to the fsinfo sector
                (free-cluster-hint #f))
           (case type
             ((fat32)
              (unless (eqv? fat32-version #x0000)
                (error 'open-fatfs "Unsupported FAT32 version" fat32-version)))
             ((exfat)
              (error 'open-fatfs "No support for exFAT")))
           (unless (= bytes/sector (bytevector-length blk0))
             (error 'open-fatfs "TODO: Support for different sector sizes"
                    bytes/sector (bytevector-length blk0)))
           (make-fatfs dev type bytes/sector sectors/cluster sectors/FAT FATs
                       first-FAT-sector
                       first-data-sector
                       total-clusters
                       fat32-root-dir-cluster
                       root-directory-sectors
                       root-directory-entries
                       codepage
                       free-cluster-hint)))))))

(define (close-fatfs fatfs)
  (dev-flush (fatfs-dev fatfs))
  (dev-close (fatfs-dev fatfs)))

(define (fatfs-volume-id fatfs)
  (define bootsector:extended-boot-signature 38)
  (define bootsector:volume-id 39)
  (define bootsector:volume-label 43)
  (define bootsector32:extended-boot-signature 66)
  (define bootsector32:volume-id 67)
  (define bootsector32:volume-label 71)
  (define extended-boot-signatures '(#x28 #x29))
  (let ((s (dev-read-sectors (fatfs-dev fatfs) 0 1)))
    (case (fatfs-type fatfs)
      ((fat12 fat16)
       (if (memv (bytevector-u8-ref s bootsector:extended-boot-signature) extended-boot-signatures)
           (bytevector-u32-ref s bootsector:volume-id (endianness little))
           #f))
      ((fat32)
       (if (memv (bytevector-u8-ref s bootsector32:extended-boot-signature) extended-boot-signatures)
           (bytevector-u32-ref s bootsector:volume-id (endianness little))
           #f))
      (else #f))))

(define (fatfs-volume-name fatfs)
  (let ((dir-reader (fatfs-directory-lister fatfs (make-root-directory-reader fatfs))))
    (let lp ()
      (let-values (((fn _lfn _cluster attr . _) (dir-reader)))
        (cond ((eof-object? fn) #f)
              ((attr-volname? attr) fn)
              (else (lp)))))))

(define (fatfs-bytes-free fatfs)
  (do ((max (fatfs-maximum-valid-cluster fatfs))
       (i (fatfs-minimum-valid-cluster fatfs) (fx+ i 1))
       (free 0 (if (eqv? 0 (fatfs-fat-ref fatfs 0 i))
                   (+ free 1)
                   free)))
      ((fx>? i max)
       (* free (fatfs-bytes/cluster fatfs)))))

(define (make-root-directory-reader fatfs)
  (case (fatfs-type fatfs)
    ((fat12 fat16)
     (make-fat12/16-root-directory-object fatfs))
    (else
     (make-cluster-object fatfs (fatfs-root-directory-cluster fatfs)))))

(define (fatfs-directory-lister fatfs reader)
  (make-directory-lister (fatfs-codepage fatfs) reader))

;; Look up the directory and directory entry for a path, which is
;; given as an absolute path as a list with no .. or . components. The
;; directory object is returned, positioned for an update or deletion.
;; If the last path component does not exist then the object is
;; positioned at the end of the directory, ready for a create (if
;; there is enough space).
(define (lookup-path who fatfs file-path last-component-must-exist?)
  (when (null? file-path)
    (error who "Internal error: called with root directory" file-path))
  (let lp-dir ((dir-reader (fatfs-directory-lister fatfs (make-root-directory-reader fatfs)))
               (path file-path))
    (cond
      ((pair? (cdr path))
       ;; Scan the directory for a subdirectory
       (let lp ()
         (let-values (((fn lfn cluster attr . _) (dir-reader)))
           (cond ((eof-object? fn)
                  (error who "Directory not found" file-path))
                 ((and (fixnum? attr) (fxtest attr FATFS-VOLUME-ID))
                  (lp))
                 ;; XXX: DOS assumes the filename on disk is upper case.
                 ((or (string-ci=? fn (car path))
                      (and lfn (string-ci=? lfn (car path))))
                  (if (attr-dir? attr)
                      (lp-dir (fatfs-directory-lister fatfs (make-cluster-object fatfs cluster))
                              (cdr path))
                      (error who "Not a directory" file-path (car path))))
                 (else (lp))))))
      (else          ;found the directory
       ;; Scan the directory for the last path component
       (let lp ()
         (let-values ([(fn lfn cluster attr file-length ctime-ms ctime cdate adate wtime wdate)
                       (dir-reader)])
           (cond ((and (fixnum? attr) (fxtest attr FATFS-VOLUME-ID))
                  (lp))
                 ((or (and (eof-object? fn) (not last-component-must-exist?))
                      (and (string? fn) (string-ci=? fn (car path)))
                      (and (string? lfn) (string-ci=? lfn (car path))))
                  (values dir-reader fn lfn cluster attr file-length
                          ctime-ms ctime cdate adate wtime wdate))
                 ((eof-object? fn)
                  (error who "File/directory not found" file-path))
                 (else (lp)))))))))

(define-record-type dir
  (sealed #t) (opaque #t)
  (fields fatfs proc))

(define (fatfs-open-directory fatfs dir-path)
  (let ((dir-proc
         (if (null? dir-path)
             (fatfs-directory-lister fatfs (make-root-directory-reader fatfs))
             (let-values ([(_dir _fn _lfn cluster . _)
                           (lookup-path 'fatfs-open-directory fatfs dir-path #t)])
               (fatfs-directory-lister fatfs (make-cluster-object fatfs cluster))))))
    (make-dir fatfs dir-proc)))

(define (fatfs-read-directory dir)
  (let ((fatfs (dir-fatfs dir))
        (dir-reader (dir-proc dir)))
    (let lp ()
      (let-values ([(fn lfn cluster attr file-length ctime-ms ctime cdate adate wtime wdate)
                    (dir-reader)])
        (cond ((eof-object? fn)
               (eof-object))
              ((attr-volname? attr)
               (lp))
              (else
               (make-fatfs-file-info fn lfn
                                     file-length
                                     (fatfs-bytes/sector fatfs)
                                     attr
                                     cluster
                                     adate wdate wtime cdate ctime ctime-ms)))))))

(define (fatfs-close-directory dir)
  #f)

(define (fatfs-file-info fatfs file-path)
  (let-values ([(_dir fn lfn cluster attr file-length ctime-ms ctime cdate adate wtime wdate)
                (lookup-path 'fatfs-file-info fatfs file-path #t)])
    (make-fatfs-file-info fn lfn
                          file-length
                          (fatfs-bytes/sector fatfs)
                          attr
                          cluster
                          adate wdate wtime cdate ctime ctime-ms)))

(define (fatfs-file-exists? fatfs file-path)
  (if (null? file-path)
      #t
      (let lp-dir ((dir-reader (fatfs-directory-lister fatfs (make-root-directory-reader fatfs)))
                   (path file-path))
        (cond
          ((pair? (cdr path))
           ;; Scan the directory for a subdirectory
           (let lp ()
             (let-values (((fn lfn cluster attr . _) (dir-reader)))
               (cond ((eof-object? fn) #f)
                     ((fxtest attr FATFS-VOLUME-ID) (lp))
                     ((or (string-ci=? fn (car path)) (and lfn (string-ci=? lfn (car path))))
                      (if (attr-dir? attr)
                          (lp-dir (fatfs-directory-lister fatfs (make-cluster-object fatfs cluster))
                                  (cdr path))
                          #f))
                     (else (lp))))))
          (else          ;found the directory
           ;; Scan the directory for the last path component
           (let lp ()
             (let-values ([(fn lfn _cluster attr . _)
                           (dir-reader)])
               (cond ((eof-object? fn) #f)
                     ((fxtest attr FATFS-VOLUME-ID) (lp))
                     ((or (string-ci=? fn (car path)) (and lfn (string-ci=? lfn (car path))))
                      #t)
                     (else (lp))))))))))

(define (fatfs-delete-file fatfs file-path)
  (define (err) (error 'fatfs-delete-file "Not a regular file" fatfs file-path))
  (when (null? file-path)
    (err))
  (let-values ([(dir _fn _lfn cluster attr . _)
                (lookup-path 'fatfs-delete-file fatfs file-path #t)])
    (unless (attr-regular? attr)
      (err))
    (dir 'delete)
    (when (<= (fatfs-minimum-valid-cluster fatfs) cluster (fatfs-maximum-valid-cluster fatfs))
      (free-cluster-chain! fatfs cluster))))

(define (fatfs-create-directory fatfs dir-path)
  (define who 'fatfs-create-directory)
  (let-values ([(parent-dir fn . _) (lookup-path who fatfs dir-path #f)])
    (cond
      ((not (eof-object? fn))
       (error 'fatfs-create-directory "Directory already exists" dir-path))
      (else
       (let* ((dir-cluster (extend-cluster-chain! fatfs 0))
              (dir-chain (make-cluster-object fatfs dir-cluster))
              (dir-lister (make-directory-lister (fatfs-codepage fatfs) dir-chain)))
         (dir-lister 'init (parent-dir 'get-start-cluster))
         (parent-dir 'create (car (reverse dir-path)) dir-cluster FATFS-DIRECTORY 0))))))

(define (fatfs-delete-directory fatfs dir-path)
  (when (null? dir-path)
    (assertion-violation 'fatfs-delete-directory "Cannot remove the root directory"
                         fatfs dir-path))
  (let-values ([(dir _fn _lfn cluster attr . _)
                (lookup-path 'fatfs-delete-directory fatfs dir-path #t)])
    (unless (attr-dir? attr)
      (error 'fatfs-delete-directory "Not a directory" fatfs dir-path))
    (let* ((dir (fatfs-open-directory fatfs dir-path))
           (entries (list (fatfs-read-directory dir)
                          (fatfs-read-directory dir)
                          (fatfs-read-directory dir))))
      (fatfs-close-directory dir)
      (unless (for-all (lambda (e)
                         (or (eof-object? e)
                             (member (fatfs-file-info:filename e) '("." ".."))))
                       entries)
        (error 'fatfs-delete-directory "Cannot remove a non-empty directory"
               fatfs dir-path)))
    (dir 'delete)
    (when (<= (fatfs-minimum-valid-cluster fatfs) cluster (fatfs-maximum-valid-cluster fatfs))
      (free-cluster-chain! fatfs cluster))))

(define (fatfs-rename-file fatfs old-path new-path)
  ;; XXX: Implementing POSIX completely is tricky here since it does
  ;; things differently depending on a trailing "/". Additional
  ;; arguments would have to be sent to this procedure by whatever
  ;; handles the pathname parsing.
  ;; https://pubs.opengroup.org/onlinepubs/9699919799/functions/rename.html
  (define who 'fatfs-rename-file)
  ;; TODO: Handle all the special cases!
  (when (or (null? old-path) (null? new-path))
    (assertion-violation who "The root directory cannot be renamed"
                         fatfs old-path new-path))
  (when (or (member (car (reverse old-path)) '("." ".."))
            (member (car (reverse new-path)) '("." "..")))
    (assertion-violation who "The . and ... entries cannot be renamed"
                         fatfs old-path new-path))
  (let-values ([(_dir _fn _lfn cluster attr file-length ctime-ms ctime cdate adate wtime wdate)
                (lookup-path who fatfs old-path #t)])
    ;; Check if the new filename exists.
    (let-values ([(new-dir new-fn* _new-lfn new-cluster new-attr . _)
                  (lookup-path who fatfs new-path #f)])
      (cond
        ((eof-object? new-fn*)         ;no, create it now
         ;; FIXME: Carry over the times
         (new-dir 'create (car (reverse new-path)) cluster attr file-length)
         (dev-flush (fatfs-dev fatfs)))
        (else                         ;it exists
         ;; FIXME: Refuse to move a directory into a subdirectory of itself
         (when (attr-dir? new-attr)
           (error who "Target is a directory" fatfs old-path new-path))
         (new-dir 'update 'cluster cluster 'attr attr 'file-length file-length)
         (when (<= (fatfs-minimum-valid-cluster fatfs) new-cluster (fatfs-maximum-valid-cluster fatfs))
           (free-cluster-chain! fatfs new-cluster))))))
  ;; Delete the old entry. Can't use the existing dir object if
  ;; new-dir is the same directory, since there is no consistent block
  ;; cache.
  (let-values ([(dir fn . _) (lookup-path who fatfs old-path #f)])
    (unless (eof-object? fn)
      (dir 'delete))))

(define fatfs-open/access-mode    #o3)
(define fatfs-open/read           #o0)
(define fatfs-open/write          #o1)
(define fatfs-open/read+write     #o2)
(define fatfs-open/create       #o100)
(define fatfs-open/exclusive    #o200)
(define fatfs-open/truncate    #o1000)
(define fatfs-open/append      #o2000)

(define (fatfs-open-file fatfs file-path flags)
  (let lp-retry ((flags flags))
    (let-values ([(dir fn lfn cluster attr file-length . _)
                  (lookup-path 'fatfs-open-file fatfs file-path #f)])
      (when (and (not (eof-object? fn)) (fxtest flags fatfs-open/exclusive))
        (error 'fatfs-open-file "File already exists" file-path flags))
      (cond
        ((and (eof-object? fn) (fxtest fatfs-open/create flags))
         (dir 'create (car (reverse file-path)) 0 FATFS-ARCHIVE 0)
         (lp-retry (fxand flags (fxnot fatfs-open/exclusive))))
        ((eof-object? fn)
         (error 'fatfs-open-file "File does not exist" file-path flags))
        (else
         (let ((mode (fxand flags fatfs-open/access-mode)))
           (when (and (fxtest attr FATFS-READ-ONLY)
                      (not (eqv? mode fatfs-open/read)))
             (error 'fatfs-open-file "Read-only file" file-path flags))
           (when (and (fxtest flags fatfs-open/truncate)
                      (or (eqv? mode fatfs-open/write)
                          (eqv? mode fatfs-open/read+write)))
             (set! file-length 0)
             (free-cluster-chain! fatfs cluster)
             (dir 'update 'cluster 0 'file-length 0))
           (cond
             ((not (attr-regular? attr))
              (error 'fatfs-open-file "Not a regular file" file-path flags))
             (else
              (let* ((port-maker
                      (cond
                        ((eqv? mode fatfs-open/read)
                         (lambda (id read! _ get-position set-position! close)
                           (make-custom-binary-input-port id read! get-position set-position! close)))
                        ((eqv? mode fatfs-open/write)
                         (lambda (id _ write! get-position set-position! close)
                           (make-custom-binary-output-port id write! get-position set-position! close)))
                        ((eqv? mode fatfs-open/read+write)
                         make-custom-binary-input/output-port)
                        (else
                         (assertion-violation 'fatfs-open-file "Invalid access mode"
                                              fatfs file-path flags))))
                     (port
                      ;; FIXME: this keeps the directory open, but it
                      ;; is not synchronized with other operations
                      ;; performed on the directory.
                      (make-fatfs-custom-port fatfs (string-append "fatfs:" (or lfn fn))
                                              cluster file-length file-path port-maker)))
                (when (fxtest flags fatfs-open/append)
                  (set-port-position! port file-length))
                port)))))))))

(define (fatfs-set-file-mode fatfs file-path mode)
  (define immutable
    (fxior FATFS-VOLUME-ID FATFS-DIRECTORY))
  (let-values ([(dir _fn _lfn _cluster attr . _)
                (lookup-path 'fatfs-set-file-mode fatfs file-path #t)])
    (let ((new-attr (fxior (fxand attr immutable)
                           (fxand mode (fxnot immutable)))))
      (dir 'update 'attr new-attr))))

(define (fatfs-set-file-timespecs fatfs file-path adate wdate wtime cdate ctime ctime-ms)
  (let ((updates (append (if adate `(adate ,adate) '())
                         (if wdate `(wdate ,wdate) '())
                         (if wtime `(wtime ,wtime) '())
                         (if cdate `(cdate ,cdate) '())
                         (if ctime `(ctime ,ctime) '())
                         (if ctime-ms `(ctime-ms ,ctime-ms) '()))))
    (let-values ([(dir _fn . _)
                  (lookup-path 'fatfs-set-file-mode fatfs file-path #t)])
      (unless (null? updates)
        (apply dir 'update updates)))))

;; Return a whole cluster list.
(define (fatfs-cluster-list fatfs cluster)
  (define (f cluster)
    (if (not (<= (fatfs-minimum-valid-cluster fatfs) cluster (fatfs-maximum-valid-cluster fatfs)))
        '()
        (let ((next (get-next-cluster fatfs cluster)))
          (if (eof-object? next)
              '()
              (cons next (f next))))))
  (cons cluster (f cluster)))

;; Get the given cluster's value in FAT #idx
(define (fatfs-fat-ref fatfs idx cluster)
  ;;(write `(fatfs-fat-ref fatfs ,idx ,cluster)) (newline)
  (unless (fx<=? (fatfs-minimum-valid-cluster fatfs) cluster (fatfs-maximum-valid-cluster fatfs))
    (assertion-violation 'fatfs-fat-ref "Invalid cluster" fatfs idx cluster))
  (let ((sector-size (fatfs-bytes/sector fatfs))
        (FAT-sector (+ (fatfs-first-FAT-sector fatfs) (* idx (fatfs-sectors/FAT fatfs)))))
    (case (fatfs-type fatfs)
      ((fat12)
       (let ((fat-offset (+ cluster (div cluster 2))))
         (let-values ([(entry-sector entry-offset) (div-and-mod fat-offset sector-size)])
           ;; XXX: Entry may live across two sectors
           (let ((FAT (dev-read-sectors (fatfs-dev fatfs)
                                        (+ FAT-sector entry-sector)
                                        2)))
             (let ((value (unpack "<uS" FAT entry-offset)))
               (if (fxodd? cluster)
                   (fxarithmetic-shift-right value 4)
                   (fxand value #xfff)))))))
      ((fat16)
       (let ((fat-offset (* cluster 2)))
         (let-values ([(entry-sector entry-offset) (div-and-mod fat-offset sector-size)])
           (let ((FAT (dev-read-sectors (fatfs-dev fatfs)
                                        (+ FAT-sector entry-sector)
                                        1)))
             (unpack "<S" FAT entry-offset)))))
      ((fat32)
       (let ((fat-offset (* cluster 4)))
         (let-values ([(entry-sector entry-offset) (div-and-mod fat-offset sector-size)])
           (let ((FAT (dev-read-sectors (fatfs-dev fatfs)
                                        (+ FAT-sector entry-sector)
                                        1)))
             (bitwise-and (unpack "<L" FAT entry-offset) #xfffffff)))))
      (else
       (error 'fatfs-fat-ref "TODO: Unsupported fs type" (fatfs-type fatfs))))))

;; Set the given cluster's value in FAT #idx to v
(define (fatfs-fat-set! fatfs idx cluster v)
  ;;(write `(fatfs-fat-set! fatfs ,idx ,cluster ,v)) (newline)
  (assert (fx<=? (fatfs-minimum-valid-cluster fatfs) cluster (fatfs-maximum-valid-cluster fatfs)))
  (let ((sector-size (fatfs-bytes/sector fatfs))
        (FAT-sector (+ (fatfs-first-FAT-sector fatfs) (* idx (fatfs-sectors/FAT fatfs)))))
    (case (fatfs-type fatfs)
      ((fat12)
       (assert (fx<=? 0 v #x0fff))
       (let ((fat-offset (+ cluster (div cluster 2))))
         (let-values ([(entry-sector entry-offset) (div-and-mod fat-offset sector-size)])
           ;; XXX: Entry may live across two sectors
           (let ((FAT (dev-read-sectors (fatfs-dev fatfs)
                                        (+ FAT-sector entry-sector)
                                        2)))
             (let* ((old (unpack "<uS" FAT entry-offset))
                    (v^ (if (fxodd? cluster)
                            (fxior (fxand old #x000f)
                                   (fxarithmetic-shift-left v 4))
                            (fxior (fxand old #xf000)
                                   v))))
               (pack! "<uS" FAT entry-offset v^)
               (dev-write-sectors (fatfs-dev fatfs) (+ FAT-sector entry-sector)
                                  FAT))))))
      ((fat16)
       (assert (fx<=? 0 v #xffff))
       (let ((fat-offset (* cluster 2)))
         (let-values ([(entry-sector entry-offset) (div-and-mod fat-offset sector-size)])
           (let ((FAT (dev-read-sectors (fatfs-dev fatfs)
                                        (+ FAT-sector entry-sector)
                                        1)))
             (pack! "<S" FAT entry-offset v)
             (dev-write-sectors (fatfs-dev fatfs)
                                (+ FAT-sector entry-sector)
                                FAT)))))
      ((fat32)
       (assert (fx<=? 0 v #x0fffffff))
       (let ((fat-offset (* cluster 4)))
         (let-values ([(entry-sector entry-offset) (div-and-mod fat-offset sector-size)])
           (let ((FAT (dev-read-sectors (fatfs-dev fatfs)
                                        (+ FAT-sector entry-sector)
                                        1)))
             (pack! "<L" FAT entry-offset v)
             (dev-write-sectors (fatfs-dev fatfs)
                                (+ FAT-sector entry-sector)
                                FAT)))))
      (else
       (error 'fatfs-fat-ref "TODO: Unsupported fs type" (fatfs-type fatfs))))))

;; Set the given cluster's value in all FATs to v
(define (fatfs-fats-set! fatfs cluster v)
  (do ((i 0 (fx+ i 1)))
      ((fx=? i (fatfs-FATs fatfs)))
    (fatfs-fat-set! fatfs i cluster v)))

;; Given a cluster, look up the next cluster in the FAT
(define (get-next-cluster fatfs cluster)
  (define who 'get-next-cluster)
  (let ((value (fatfs-fat-ref fatfs 0 cluster)))
    (case (fatfs-type fatfs)
      ((fat12)
       (cond ((or (fx>=? value #xff8) (fx<=? value #x001))
              (eof-object))
             ((fx=? value #xff7)
              (error who "Next cluster is marked as bad" cluster))
             (else
              value)))
      ((fat16)
       (cond ((or (fx>=? value #xfff8) (fx<=? value #x0001))
              (eof-object))
             ((fx=? value #xfff7)
              (error who "Next cluster is marked as bad" cluster))
             (else
              value)))
      ((fat32)
       (cond ((or (fx>=? value #xffffff8) (fx<=? value #x0000001))
              (eof-object))
             ((fx=? value #xffffff7)
              (error who "Next cluster is marked as bad" cluster))
             (else
              value)))
      (else
       (error who "Unsupported fs type" (fatfs-type fatfs))))))

;; Find a free cluster. Starts looking at the hinted cluster, but
;; starts from the start of the FAT if none is available. Worst case
;; is a complete scan of the FAT.
(define (find-free-cluster fatfs cluster-hint)
  (define (scan start)
    (let ((max (fatfs-maximum-valid-cluster fatfs)))
      (let lp ((i start))
        (cond ((fx=? i max) #f)
              ((eqv? 0 (fatfs-fat-ref fatfs 0 i)) i)
              (else (lp (fx+ i 1)))))))
  (let* ((cluster-hint (or cluster-hint (fatfs-free-cluster-hint fatfs)))
         (cluster
          (if (and cluster-hint
                   (<= (fatfs-minimum-valid-cluster fatfs) cluster-hint
                       (fatfs-maximum-valid-cluster fatfs)))
              (or (scan cluster-hint) (scan (fatfs-minimum-valid-cluster fatfs)))
              (scan (fatfs-minimum-valid-cluster fatfs)))))
    (fatfs-free-cluster-hint-set! fatfs cluster)
    cluster))

;; Extend a cluster chain with an additional cluster. The given
;; cluster is updated to point to the new cluster, unless it was an
;; empty cluster.
(define (extend-cluster-chain! fatfs cluster)
  (define who 'extend-cluster-chain!)
  (unless (or (< cluster 2) (eof-object? (get-next-cluster fatfs cluster)))
    (assertion-violation who "Not at an end of cluster" fatfs cluster))
  (let ((new-cluster (find-free-cluster fatfs cluster)))
    (when (not new-cluster)
      (error who "No free clusters" fatfs cluster))
    (let ((eoc (fatfs-end-of-cluster-chain fatfs)))
      (when (>= cluster 2)
        (fatfs-fats-set! fatfs cluster new-cluster))
      (fatfs-fats-set! fatfs new-cluster eoc))
    new-cluster))

;; Free the cluster chain starting with the first given cluster. XXX:
;; If this is the whole chain for a file then the directory entry must
;; be updated to point to cluster 0. Otherwise the last now-valid
;; cluster must be updated to contain EOC.
(define (free-cluster-chain! fatfs cluster)
  (let lp ((cluster cluster))
    (unless (< cluster 2)
      (let ((next (get-next-cluster fatfs cluster)))
        (fatfs-fats-set! fatfs cluster 0)
        (unless (eof-object? next)
          (lp next))))))

(define (fatfs-truncate-file fatfs file-path len)
  (define who 'fatfs-truncate-file)
  (let-values ([(dir _fn _lfn cluster attr file-length . _) (lookup-path who fatfs file-path #t)])
    (when (fxtest attr FATFS-READ-ONLY)
      (error who "Read-only file" fatfs file-path len))
    (when (fxtest attr FATFS-DIRECTORY)
      (error who "Expected a file but got a directory" fatfs file-path len))
    (cond
      ((= len file-length) #f)          ;nothing to do
      ((eqv? len 0)
       ;; Free the cluster chain
       (free-cluster-chain! fatfs cluster)
       (dir 'update 'cluster 0 'file-length 0))
      (else
       (let* ((cluster-size (fatfs-bytes/cluster fatfs))
              (new-cluster-length (div (+ len (fx- cluster-size 1)) cluster-size))
              (start-cluster (if (< cluster 2)
                                  (extend-cluster-chain! fatfs cluster)
                                  cluster)))
         ;; Grow/shrink the cluster chain and then update the
         ;; directory entry
         (let lp ((i 1) (cluster start-cluster))
           (let ((next-cluster (get-next-cluster fatfs cluster)))
             (cond
               ((fx=? i new-cluster-length)
                (unless (eof-object? next-cluster)
                  (fatfs-fats-set! fatfs cluster (fatfs-end-of-cluster-chain fatfs))
                  (free-cluster-chain! fatfs next-cluster))
                (dir 'update 'cluster start-cluster 'file-length len))
               (else
                (if (eof-object? next-cluster)
                    (let ((new-cluster (extend-cluster-chain! fatfs cluster)))
                      ;; FIXME: Erase the sectors and the slack
                      (lp (fx+ i 1) new-cluster))
                    (lp (fx+ i 1) next-cluster)))))))))))

(define (make-fat12/16-root-directory-object fatfs)
  (define start-sector
    (fx- (fatfs-first-data-sector fatfs)
         (fatfs-root-directory-sectors fatfs)))
  (define end-sector
    (fx+ start-sector (fatfs-root-directory-sectors fatfs)))
  (define sector start-sector)
  (case-lambda
    ((cmd)
     (case cmd
       ((get-sector)
        (cond
          ((fx=? sector end-sector)
           (eof-object))
          (else
           (dev-read-sectors (fatfs-dev fatfs) sector 1))))
       ((step-forward)
        (unless (fx=? sector end-sector)
          (set! sector (+ sector 1))))
       ((rewind)
        (set! sector start-sector))
       ((extend!)
        (assertion-violation #f "FAT12/FAT16 root directories are fixed size"))
       ((at-end?)
        (fx=? sector end-sector))
       ((space-available?)
        #f)
       ((get-position) (fxdiv sector (fatfs-bytes/sector fatfs)))
       ((get-start-cluster) 0)
       (else
        (error #f "Bad command" cmd))))
    ((cmd arg)
     (case cmd
       ((put-sector)
        (assert (and (bytevector? arg)
                     (eqv? (bytevector-length arg) (fatfs-bytes/sector fatfs))))
        (when (fx=? sector end-sector)
          (error #f "Cannot write past the end of the root directory"))
        (dev-write-sectors (fatfs-dev fatfs) sector arg))
       ((set-position)
        (let ((to (fx+ start-sector (fx* arg (fatfs-bytes/sector fatfs)))))
          (assert (and (fx>=? to start-sector) (fx<? to end-sector)))
          (set! sector to)))
       (else
        (error #f "Bad command" cmd))))))

;; Access a cluster chain, one sector at a time. The operations are:
;; read a sector, write a sector, step forward to the next sector,
;; rewind to the beginning, extend the chain, sense end of chain.
(define (make-cluster-object fatfs start-cluster)
  ;; Given a cluster number, return its sector index on the media.
  (define (cluster->sector-index fatfs n)
    (assert (>= n 2))
    (+ (* (- n 2) (fatfs-sectors/cluster fatfs))
       (fatfs-first-data-sector fatfs)))
  (define previous-cluster #f)
  (define cluster start-cluster)        ;current cluster
  (define sector 0)                     ;sector within the current cluster
  (define sector-idx 0)
  (define cluster-obj
    (case-lambda
      ((cmd)
       (case cmd
         ((get-sector)
          (cond
            ((or (eof-object? cluster) (< cluster (fatfs-minimum-valid-cluster fatfs)))
             (eof-object))
            (else
             (dev-read-sectors (fatfs-dev fatfs)
                               (+ (cluster->sector-index fatfs cluster) sector)
                               1))))
         ((step-forward)
          (unless (or (eof-object? cluster) (< cluster (fatfs-minimum-valid-cluster fatfs)))
            (cond ((fx=? sector (fx- (fatfs-sectors/cluster fatfs) 1))
                   (set! previous-cluster cluster)
                   (set! cluster (get-next-cluster fatfs cluster))
                   (set! sector 0))
                  (else
                   (set! sector (fx+ sector 1))))
            (set! sector-idx (fx+ sector-idx 1))))
         ((rewind)
          (set! previous-cluster #f)
          (set! cluster start-cluster)
          (set! sector 0)
          (set! sector-idx 0))
         ((extend!)
          (when (eqv? cluster 0)
            (assertion-violation #f "Attempt to extend the empty cluster chain"))
          (unless (eof-object? cluster)
            (assertion-violation #f "Must be positioned at the end of the cluster chain"))
          (set! cluster (extend-cluster-chain! fatfs previous-cluster))
          ;; FAT does not have sparse files but most programs that seek
          ;; into a file assume that the unwritten parts are filled with
          ;; zeros. The directory code assumes this as well.
          (do ((zero (make-bytevector (fatfs-bytes/sector fatfs) 0))
               (i 0 (fx+ i 1)))
              ((fx=? i (fatfs-sectors/cluster fatfs)))
            (dev-write-sectors (fatfs-dev fatfs)
                               (+ (cluster->sector-index fatfs cluster) i)
                               zero)))
         ((at-end?)
          (eof-object? cluster))
         ((space-available?)
          (and (find-free-cluster fatfs #f) #t))
         ((get-position)
          sector-idx)
         ((get-start-cluster) start-cluster)
         (else (error #f "Bad command" cmd))))
      ((cmd arg)
       (case cmd
         ((put-sector)
          (assert (and (bytevector? arg)
                       (eqv? (bytevector-length arg)
                             (fatfs-bytes/sector fatfs))))
          (when (eqv? cluster 0)
            (error #f "Cannot write to the empty cluster chain"))
          (when (eof-object? cluster)
            (error #f "Cannot write past the end of the cluster"))
          (dev-write-sectors (fatfs-dev fatfs)
                             (+ (cluster->sector-index fatfs cluster) sector)
                             arg))
         ((set-position)
          (unless (= sector-idx arg)
            (cluster-obj 'rewind)
            (do ((i 0 (fx+ i 1)))
                ((fx=? i arg))
              (cluster-obj 'step-forward))))
         (else (error #f "Bad command" cmd arg))))))
  cluster-obj)

;; Create a binary input/output port that is hooked up to a cluster
;; chain. It is optionally connected to a filename. If it has a
;; filename then its directory entry is updated as appropriate.
;; Otherwise only the cluster chain is updated.
(define (make-fatfs-custom-port fatfs id cluster file-length file-path make)
  (define file-position 0)
  (define sector-data #f)
  (define sector-position 0)
  (define direntry-dirty? #f)
  (define chain (make-cluster-object fatfs cluster))
  (define chain-sector-pos 0)
  (define (seek)
    ;; Position the chain on the sector of the file position, if
    ;; possible.
    (let-values ([(file-sector-pos offset) (div-and-mod file-position (fatfs-bytes/sector fatfs))])
      (set! sector-position offset)
      (unless (= chain-sector-pos file-sector-pos)
        (set! sector-data #f)
        (when (> chain-sector-pos file-sector-pos)
          ;; The FAT does not maintain backward links
          (chain 'rewind)
          (set! chain-sector-pos 0))
        (let lp ()
          (unless (or (= chain-sector-pos file-sector-pos)
                      (chain 'at-end?))
            (chain 'step-forward)
            (set! chain-sector-pos (+ chain-sector-pos 1))
            (lp))))))
  (define (read! bv start count)
    (seek)
    (when (not sector-data)
      (set! sector-data (chain 'get-sector)))
    (if (eof-object? sector-data)
        0
        (let* ((remaining-bytes (and file-length (- file-length file-position)))
               (bytes-read (min count
                                (fx- (bytevector-length sector-data)
                                     sector-position)
                                (or remaining-bytes count))))
          (bytevector-copy! sector-data sector-position
                            bv start bytes-read)
          (set! sector-position (fx+ sector-position bytes-read))
          (set! file-position (+ file-position bytes-read))
          bytes-read)))
  (define (write! bv start count)
    (assert (not (eqv? count 0)))
    (when (eof-object? sector-data)
      (set! sector-data #f))
    (when (eqv? cluster 0)
      ;; Allocate the first cluster for this file.
      (let ((free-cluster (extend-cluster-chain! fatfs cluster)))
        (set! cluster free-cluster)
        (set! chain (make-cluster-object fatfs free-cluster))))
    (let lp ()
      ;; Extend the chain until there is space to write more data at
      ;; the current file position.
      (seek)
      (when (chain 'at-end?)
        (chain 'extend!)
        (lp)))
    (let ((count (fxmin count (fx- (fatfs-bytes/sector fatfs) sector-position))))
      (when (not sector-data)
        ;; No buffered sector. The media can only write whole sectors.
        ;; If the port is aligned to a sector boundary and there is
        ;; enough data to write an entire sector then there is no need
        ;; to read back a partial sector.
        (if (and (eqv? sector-position 0)
                 (= count (fatfs-bytes/sector fatfs)))
            (set! sector-data (make-bytevector (fatfs-bytes/sector fatfs) 0))
            (set! sector-data (chain 'get-sector))))
      ;; Copy data into the sector and write it to the medium. This
      ;; might result in multiple writes if the port abstraction
      ;; doesn't keep the position aligned properly, but that can't be
      ;; helped.
      (bytevector-copy! bv start
                        sector-data sector-position
                        count)
      (chain 'put-sector sector-data)
      (set! sector-position (+ sector-position count))
      (set! file-position (+ file-position count))
      (when file-length
        (set! direntry-dirty? #t)
        (set! file-length (max file-position file-length)))
      count))
  (define (get-position)
    file-position)
  (define (set-position! pos)
    (set! file-position pos))
  (define (close)
    (when direntry-dirty?
      (let-values ([(dir fn  . _) (lookup-path 'close-port fatfs file-path #t)])
        (unless (eof-object? fn)
          ;; TODO: Last write date and time
          (dir 'update 'cluster cluster 'file-length file-length)
          (dev-flush (fatfs-dev fatfs))
          (set! direntry-dirty? #f)))))
  (make id read! write! get-position set-position! close)))
