#!/bin/sh
set -e

setup () {
    dd if=/dev/zero of=test.img bs=512 seek=$(( $1 - 1 )) count=1
    mformat -S 2 -i test.img
    mcopy -i test.img README.md ::
    mcopy -i test.img fatfs.sls ::
    mcopy -i test.img run-tests.sh ::
    mmd -i test.img empty
    mmd -i test.img unempty
    mcopy -i test.img README.md ::/unempty/
}

setup 2880
tests/test-fatfs.sps
if ! command -v larceny 2>/dev/null; then
    tools/fatfs-type.sps test.img "HELLO.TXT"
fi

setup 65536
tests/test-fatfs.sps

setup $(( ( 3 * 1024 * 1024 * 1024 ) / 512 ))
tests/test-fatfs.sps

if command -v loko 2>/dev/null; then
    loko -feval -ftarget=linux --compile tests/test-fatfs.sps --output tests/test-fatfs
    setup 2880
    tests/test-fatfs
fi
